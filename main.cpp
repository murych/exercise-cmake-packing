#include <QApplication>
#include <frontend/mainwindow.h>

auto
main(int argc, char* argv[]) -> int
{
  QApplication app{ argc, argv };
  auto window{ std::make_unique<main_window>() };
  window->show();

  const auto ret_status = QApplication::exec();
  return ret_status;
}
