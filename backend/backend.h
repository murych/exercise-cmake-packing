#ifndef BACKEND_APPLICATION_BACKEND_H
#define BACKEND_APPLICATION_BACKEND_H

#include <QString>

namespace backend {

class application_backend
{

public:
  void create_archive(const QString& path);

private:
  void generate(const QString& path) const;
};

} // namespace backend

#endif // BACKEND_APPLICATION_BACKEND_H
