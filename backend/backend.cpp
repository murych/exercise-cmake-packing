#include <QDataStream>
#include <QDebug>
#include <QTemporaryDir>

#include "backend.h"

#include <JlCompress.h>
#include <core/worker.h>

namespace backend
{

void
application_backend::create_archive(const QString& path)
{
  QTemporaryDir m_work_dir;
  if (!m_work_dir.isValid()) {
    return;
  }

  generate(m_work_dir.path());

  QDir dir{ m_work_dir.path() };
  const auto list_infos{ dir.entryInfoList(QDir::Files |
                                           QDir::NoDotAndDotDot) };
  QStringList list;
  for (const auto& file : list_infos) {
    list.append(file.absoluteFilePath());
  }

  if (!JlCompress::compressFiles(path, list)) {
    qWarning() << "couldn't create archive" << path;
    return;
  }

  qInfo() << "created archive" << path;
}

void
application_backend::generate(const QString& path) const
{
  const auto max_files{ 50 };

  for (auto idx = 0; idx < max_files; ++idx) {
    QFileInfo file_info{ path + QStringLiteral("/") + QString::number(idx) +
                         QStringLiteral(".txt") };
    QFile file{ file_info.absoluteFilePath() };
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
      qWarning() << "couldn't open file" << file_info.absoluteFilePath();
      return;
    }
    const auto worker{ std::make_unique<core::worker>() };
    const auto data{ worker->generate_numbers(100) };

    QDataStream stream {&file};
    stream.setVersion(QDataStream::Qt_5_15);
    stream.setByteOrder(QDataStream::BigEndian);

    stream.writeRawData(data.data(), data.size());

    file.close();

    qDebug() << "written file" << file_info.absoluteFilePath() << data;
  }
}

} // namespace backend
