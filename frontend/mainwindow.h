#ifndef FRONTEND_MAINWINDOW_H
#define FRONTEND_MAINWINDOW_H

#include <QMainWindow>

namespace backend {
class application_backend;
} // namespace backend

namespace Ui {
class MainWindow;
} // namespace Ui

class main_window : public QMainWindow
{
  Q_OBJECT

public:
  explicit main_window(QWidget* parent = nullptr);
  ~main_window() override;

private:
  std::unique_ptr<Ui::MainWindow> m_ui{ nullptr };
  std::unique_ptr<backend::application_backend> m_backend{ nullptr };

  QString m_target_path;

  void choose_target_path();
};

#endif // FRONTEND_MAINWINDOW_H
