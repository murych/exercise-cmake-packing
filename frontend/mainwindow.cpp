#include "mainwindow.h"

#include <QFileDialog>
#include <QStandardPaths>
#include <QTemporaryDir>
#include <backend/backend.h>

#include "ui_mainwindow.h"

main_window::main_window(QWidget* parent)
  : QMainWindow{ parent }
  , m_ui{ std::make_unique<Ui::MainWindow>() }
  , m_backend{ std::make_unique<backend::application_backend>() }
  , m_target_path{ QStandardPaths::HomeLocation }
{
  m_ui->setupUi(this);

  connect(m_ui->b_choose,
          &QPushButton::clicked,
          this,
          &::main_window::choose_target_path);

  connect(m_ui->b_create, &QPushButton::clicked, this, [this] {
    m_backend->create_archive(m_target_path);
  });
}

void
main_window::choose_target_path()
{
  auto dialog{ std::make_unique<QFileDialog>() };
  dialog->setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
  dialog->setFileMode(QFileDialog::Directory);
  dialog->setOption(QFileDialog::ShowDirsOnly, /*on=*/true);

  QStringList file_names;
  if (dialog->exec() != 0) {
    file_names = dialog->selectedFiles();
  }

  m_target_path = (file_names.isEmpty() ? QApplication::applicationFilePath()
                                        : file_names.at(0)) +
                  QStringLiteral("/archive.zip");
}

main_window::~main_window() = default;
