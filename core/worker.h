#ifndef CORE_WORKER_H
#define CORE_WORKER_H

#include <cstdlib>
#include <ctime>
#include <vector>

namespace core
{

class worker
{
public:
  static auto generate_numbers(int size) -> std::vector<char>
  {
    std::srand(std::time(nullptr));
    std::vector<char> result;
    result.reserve(size);

    for (int idx = 0; idx < size; ++idx) {
      result.push_back(std::rand());
    }

    return result;
  }
};

}  // namespace core

#endif // CORE_WORKER_H
